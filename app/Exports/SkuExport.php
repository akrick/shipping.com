<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;


class SkuExport implements FromArray,WithHeadings,WithStrictNullComparison
{

    protected $records;

    public function __construct(array $records)
    {
        $this->records = $records;
    }
    public function headings(): array
    {
        return [
            '产品代号',
            '产品名称',
            '仓库',
            '采购在途',
            '可用/可售',
            '预警',
            '可售天数',
            '缺货',
            '缺货天数',
            '预计到货'
        ];
    }
    public function array(): array
    {
        return $this->records;
    }
}
