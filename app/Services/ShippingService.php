<?php
namespace App\Services;
use QL\QueryList;
use QL\Ext\CurlMulti;
use Illuminate\Support\Facades\Log;
use function MongoDB\BSON\toJSON;

/**
 * 运费集成查询服务
 * Class ShippingService
 * @package App\Services
 */
class ShippingService{

    private $ql;
    public function __construct()
    {
        $this->ql = QueryList::getInstance();
        $this->ql->use(CurlMulti::class);
    }

    /**
     * 获取毅达物流运转方式
     */
    public function fetchYdMethods(){
        $data = array();
        $hashkey = md5('fetchYdMethods');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('http://yd.a-56.com:5903/userLogin/go',[
                'userid' => 'YX5075',
                'password' => 'YD123456'
            ])->get('http://yd.a-56.com:5903/Price/CalPriceExp');
            $html = $this->ql->encoding('UTF-8')->find("#ddlTransit>option")->texts();
            $data = $html->toArray();
            array_shift($data);

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取毅达包裹类型
     */
    public function fetchYdTypes(){

        $data = array();
        $hashkey = md5('fetchYdTypes');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = json_decode($jsonstr, true);
        }else{
            $this->ql->post('http://yd.a-56.com:5903/userLogin/go',[
                'userid' => 'YX5075',
                'password' => 'YD123456'
            ])->get('http://yd.a-56.com:5903/Price/CalPriceExp');
            $html = $this->ql->find("#ddlPackDoc>option")->htmls();
            $data = $html->toArray();

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取毅达物流目的地国家列表
     */
    public function fetchYdCountries(){

        $data = array();
        $hashkey = md5('fetchYdCountries');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('http://yd.a-56.com:5903/userLogin/go',[
                'userid' => 'YX5075',
                'password' => 'YD123456'
            ])->get('http://yd.a-56.com:5903/Price/CalPriceExp');

            //获取目的地列表
            $post_data = [
                'module' => 'Search',
                'method' => 'country',
                'PageSize' => 1000,
                'PageIndex' => 1,
                'first' => 0,
                'guest' => '',
                'Company' => 'YD',
                'type' => 1
            ];
            $this->ql->post('http://yd.a-56.com:5903/ashx/Search.ashx', $post_data);
            $content =  $this->ql->getHtml();
            $result = json_decode($content, true);
            $rdata = $result['Tab'][0]['Data'];

            foreach ($rdata as $val){
                $item = array();
                $item['id'] = $val['ID'];
                $item['name_lt'] = $val['c1'];
                $item['name_en'] = $val['c2'];
                $item['name_cn'] = $val['c3'];
                $data[] = $item;
            }

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取毅达物流价格查询列表
     * @param string $country_no 国家(必填)
     * @param int $cwt 重量（必填）
     * @param string $type 类型
     * @param string $method 运转方式
     * @return string
     */
    public function fetchYd($country_no = '', $cwt = 0, $method = '', $type = 'WPX'){

        $this->ql->post('http://yd.a-56.com:5903/userLogin/go',[
            'userid' => 'YX5075',
            'password' => 'YD123456'
        ])->get('http://yd.a-56.com:5903/Price/CalPriceExp');
//        form表单
//        http://yd.a-56.com:5903/ashx/a2.ashx
//        module: CalPrice
//        method: CalPrice
//        guest:
//        Company: YD
//        CountryNO: AF
//        CWT: 10
//        PackDoc: WPX
//        Transit: DHL
//        dataType: tab
        $post_data = [
            'module' => 'CalPrice',
            'method' => 'CalPrice',
            'guest' => '',
            'Company' => 'YD',
            'CountryNO' => $country_no,
            'CWT' => $cwt, //重量
            'PackDoc' => $type,
            'Transit' => $method,
            'dataType' => 'tab'
        ];
        $rs = $this->ql->post('http://yd.a-56.com:5903/ashx/a2.ashx', $post_data);
        $html = $this->ql->getHtml(true);        
        $result = json_decode($html, true);
        if(!$result){
            $html = strip_tags($html);
            $html = substr($html, 0, -1);
            $result = json_decode($html, true);
            if(!$result){
                $result = json_decode($html."\"}]}]}", true);
            }
        }
//        dd($result);
        $data = array();
        if(!empty($result)) {
            foreach ($result['Tab']['0']['Data'] as $key => $val) {
                $item = array();
                $item['channel'] = '毅如达';
//                $item['guest_type'] = $val['a'];
                $item['name'] = $val['g'];
                $item['transit_method'] = $val['b'];
                $item['weight'] = $val['d']; //KG
//                $item['f'] = $val['f'];
//                $item['volume'] = $val['h'];
                $item['total_price'] = floatval($val['j']);
//                $item['price_type'] = $val['k'];
//                $item['n'] = $val['n'];
                $item['note'] = $val['o'];
                $data[] = $item;
            }
        }
        return $data;

    }

    /**
     * 获取升蓝货物类型
     */
    public function fetchSlTypes(){

        $data = array();
        $hashkey = md5('fetchSlTypes');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('https://www.sl56.com/Account/LogOn',[
                'Username' => 'CMHGZ8',
                'Password' => 'homary123'
            ])->get('https://signalr.sl56.com/signalr/hubs');
            $this->ql->post('https://www.sl56.com/Agreement/Agree');
            $this->ql->get('https://www.sl56.com/Calculation');

            $vals = $this->ql->find("#productTypeList>li>a")->htmls()->toArray();
            $keys = $this->ql->find("#productTypeList>li>a")->attrs('producttype')->toArray();
            $data = array_combine($keys, $vals);

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取升蓝运输方式
     */
    public function fetchSlMethods(){

        $data = array();
        $hashkey = md5('fetchSlMethods');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('https://www.sl56.com/Account/LogOn',[
                'Username' => 'CMHGZ8',
                'Password' => 'homary123'
            ])->get('https://signalr.sl56.com/signalr/hubs');
            $this->ql->post('https://www.sl56.com/Agreement/Agree');
            $this->ql->get('https://www.sl56.com/Calculation');

            $vals = $this->ql->find("#modeOfTransportList>li>a")->htmls()->toArray();
            $keys = $this->ql->find("#modeOfTransportList>li>a")->attrs('modeoftransportid')->toArray();
            $data = array_combine($keys, $vals);

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;

    }

    /**
     * 获取升蓝国家列表
     */
    public function fetchSlCountries(){

        $data = array();
        $hashkey = md5('fetchSlCountries');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $json_data = '[{"Id":1,"Name":"AD 安道尔共和国","UsePostalcode":true},{"Id":2,"Name":"AE 阿联酋","UsePostalcode":false},{"Id":3,"Name":"AF 阿富汗","UsePostalcode":false},{"Id":4,"Name":"AG 安提瓜和巴布达","UsePostalcode":false},{"Id":5,"Name":"AI 安圭拉","UsePostalcode":false},{"Id":6,"Name":"AL 阿尔巴尼亚","UsePostalcode":false},{"Id":7,"Name":"AM 亚美尼亚","UsePostalcode":true},{"Id":8,"Name":"AN 荷属安的列斯群岛","UsePostalcode":false},{"Id":9,"Name":"AO 安哥拉","UsePostalcode":false},{"Id":10,"Name":"AR 阿根廷","UsePostalcode":true},{"Id":11,"Name":"AS 美属萨摩亚群岛","UsePostalcode":true},{"Id":12,"Name":"AT 奥地利","UsePostalcode":true},{"Id":13,"Name":"AU 澳大利亚","UsePostalcode":true},{"Id":14,"Name":"AW 阿鲁巴岛","UsePostalcode":false},{"Id":15,"Name":"AZ 阿塞拜疆","UsePostalcode":true},{"Id":16,"Name":"BA 波黑共和国","UsePostalcode":true},{"Id":17,"Name":"BB 巴巴多斯","UsePostalcode":false},{"Id":18,"Name":"BD 孟加拉","UsePostalcode":true},{"Id":19,"Name":"BE 比利时","UsePostalcode":true},{"Id":20,"Name":"BF 布基纳法索","UsePostalcode":false},{"Id":21,"Name":"BG 保加利亚","UsePostalcode":true},{"Id":22,"Name":"BH 巴林","UsePostalcode":false},{"Id":23,"Name":"BI 布隆迪","UsePostalcode":false},{"Id":24,"Name":"BJ 贝宁","UsePostalcode":false},{"Id":25,"Name":"BM 百慕大群岛","UsePostalcode":false},{"Id":26,"Name":"BN 文莱","UsePostalcode":true},{"Id":27,"Name":"BO 玻利维亚","UsePostalcode":false},{"Id":28,"Name":"BR 巴西","UsePostalcode":true},{"Id":29,"Name":"BS 巴哈马","UsePostalcode":false},{"Id":30,"Name":"BT 不丹","UsePostalcode":false},{"Id":31,"Name":"BW 博茨瓦纳","UsePostalcode":false},{"Id":32,"Name":"BY 白俄罗斯","UsePostalcode":true},{"Id":33,"Name":"BZ 伯利兹","UsePostalcode":false},{"Id":34,"Name":"CA 加拿大","UsePostalcode":true},{"Id":35,"Name":"CD 刚果民主共和国(金)","UsePostalcode":true},{"Id":36,"Name":"CF 中非共和国","UsePostalcode":false},{"Id":37,"Name":"CG 刚果共和国(布)","UsePostalcode":false},{"Id":38,"Name":"CH 瑞士","UsePostalcode":true},{"Id":39,"Name":"CI 科特迪瓦","UsePostalcode":false},{"Id":40,"Name":"CK 库克群岛","UsePostalcode":false},{"Id":41,"Name":"CL 智利","UsePostalcode":false},{"Id":42,"Name":"CM 喀麦隆","UsePostalcode":false},{"Id":43,"Name":"CN 中国","UsePostalcode":true},{"Id":44,"Name":"CO 哥伦比亚","UsePostalcode":true},{"Id":45,"Name":"CR 哥斯达黎加","UsePostalcode":false},{"Id":46,"Name":"CU 古巴","UsePostalcode":true},{"Id":47,"Name":"CV 佛得角","UsePostalcode":false},{"Id":48,"Name":"CY 塞浦路斯","UsePostalcode":true},{"Id":49,"Name":"CZ 捷克","UsePostalcode":true},{"Id":50,"Name":"DE 德国","UsePostalcode":true},{"Id":51,"Name":"DJ 吉布提","UsePostalcode":false},{"Id":52,"Name":"DK 丹麦","UsePostalcode":true},{"Id":53,"Name":"DM 多米尼克","UsePostalcode":false},{"Id":54,"Name":"DO 多米尼加共和国","UsePostalcode":false},{"Id":55,"Name":"DZ 阿尔及利亚","UsePostalcode":true},{"Id":56,"Name":"EC 厄瓜多尔","UsePostalcode":true},{"Id":57,"Name":"EE 爱沙尼亚","UsePostalcode":true},{"Id":58,"Name":"EG 埃及","UsePostalcode":false},{"Id":59,"Name":"ER 厄立特里亚","UsePostalcode":false},{"Id":60,"Name":"ES 西班牙","UsePostalcode":true},{"Id":61,"Name":"ET 埃塞俄比亚","UsePostalcode":false},{"Id":62,"Name":"FI 芬兰","UsePostalcode":true},{"Id":63,"Name":"FJ 斐济","UsePostalcode":false},{"Id":64,"Name":"FK 福克兰群岛","UsePostalcode":false},{"Id":65,"Name":"FM 密克罗尼西亚联邦国","UsePostalcode":true},{"Id":66,"Name":"FO 法罗群岛","UsePostalcode":true},{"Id":67,"Name":"FR 法国","UsePostalcode":true},{"Id":68,"Name":"GA 加蓬","UsePostalcode":false},{"Id":69,"Name":"GB 英国","UsePostalcode":true},{"Id":70,"Name":"GD 格林纳达","UsePostalcode":false},{"Id":71,"Name":"GE 格鲁吉亚","UsePostalcode":true},{"Id":72,"Name":"GF 法属圭亚那","UsePostalcode":true},{"Id":73,"Name":"GG 根西岛","UsePostalcode":true},{"Id":74,"Name":"GH 加纳","UsePostalcode":false},{"Id":75,"Name":"GI 直布罗陀","UsePostalcode":false},{"Id":76,"Name":"GL 格陵兰","UsePostalcode":true},{"Id":77,"Name":"GM 冈比亚","UsePostalcode":false},{"Id":78,"Name":"GN 几内亚","UsePostalcode":false},{"Id":79,"Name":"GP 瓜德罗普岛","UsePostalcode":true},{"Id":80,"Name":"GQ 赤道几内亚","UsePostalcode":false},{"Id":81,"Name":"GR 希腊","UsePostalcode":true},{"Id":82,"Name":"GT 危地马拉","UsePostalcode":false},{"Id":83,"Name":"GU 关岛","UsePostalcode":true},{"Id":84,"Name":"GW 几内亚比绍","UsePostalcode":false},{"Id":85,"Name":"GY 圭亚那","UsePostalcode":false},{"Id":86,"Name":"HK 香港","UsePostalcode":false},{"Id":87,"Name":"HN 洪都拉斯","UsePostalcode":false},{"Id":88,"Name":"HR 克罗地亚","UsePostalcode":true},{"Id":89,"Name":"HT 海地","UsePostalcode":false},{"Id":90,"Name":"HU 匈牙利","UsePostalcode":true},{"Id":91,"Name":"IC 加那利群岛","UsePostalcode":true},{"Id":92,"Name":"ID 印度尼西亚","UsePostalcode":true},{"Id":93,"Name":"IE 爱尔兰","UsePostalcode":false},{"Id":94,"Name":"IL 以色列","UsePostalcode":true},{"Id":95,"Name":"IN 印度","UsePostalcode":true},{"Id":96,"Name":"IQ 伊拉克","UsePostalcode":false},{"Id":97,"Name":"IR 伊朗","UsePostalcode":false},{"Id":98,"Name":"IS 冰岛","UsePostalcode":true},{"Id":99,"Name":"IT 意大利","UsePostalcode":true},{"Id":100,"Name":"JE 泽西岛","UsePostalcode":true},{"Id":101,"Name":"JM 牙买加","UsePostalcode":false},{"Id":102,"Name":"JO 约旦","UsePostalcode":false},{"Id":103,"Name":"JP 日本","UsePostalcode":true},{"Id":104,"Name":"KE 肯尼亚","UsePostalcode":false},{"Id":105,"Name":"KG 吉尔吉斯斯坦","UsePostalcode":true},{"Id":106,"Name":"KH 柬埔寨","UsePostalcode":true},{"Id":107,"Name":"KI 基里巴斯","UsePostalcode":false},{"Id":108,"Name":"KM 科摩罗","UsePostalcode":false},{"Id":109,"Name":"KN 圣基茨","UsePostalcode":false},{"Id":110,"Name":"KP 朝鲜","UsePostalcode":false},{"Id":111,"Name":"KR 韩国","UsePostalcode":true},{"Id":112,"Name":"KV 科索沃","UsePostalcode":true},{"Id":113,"Name":"KW 科威特","UsePostalcode":false},{"Id":114,"Name":"KY 开曼群岛","UsePostalcode":false},{"Id":115,"Name":"KZ 哈萨克斯坦","UsePostalcode":true},{"Id":116,"Name":"LA 老挝","UsePostalcode":false},{"Id":117,"Name":"LB 黎巴嫩","UsePostalcode":false},{"Id":118,"Name":"LC 圣卢西亚","UsePostalcode":false},{"Id":119,"Name":"LI 列支敦士登","UsePostalcode":true},{"Id":120,"Name":"LK 斯里兰卡","UsePostalcode":false},{"Id":121,"Name":"LR 利比里亚","UsePostalcode":false},{"Id":122,"Name":"LS 莱索托","UsePostalcode":false},{"Id":123,"Name":"LT 立陶宛","UsePostalcode":true},{"Id":124,"Name":"LU 卢森堡","UsePostalcode":true},{"Id":125,"Name":"LV 拉脱维亚","UsePostalcode":true},{"Id":126,"Name":"LY 利比亚","UsePostalcode":false},{"Id":127,"Name":"MA 摩洛哥","UsePostalcode":true},{"Id":128,"Name":"MC 摩纳哥","UsePostalcode":true},{"Id":129,"Name":"MD 摩尔多瓦","UsePostalcode":true},{"Id":130,"Name":"ME 黑山共和国","UsePostalcode":true},{"Id":131,"Name":"MG 马达加斯加","UsePostalcode":true},{"Id":132,"Name":"MH 马绍尔群岛","UsePostalcode":true},{"Id":133,"Name":"MK 马其顿","UsePostalcode":true},{"Id":134,"Name":"ML 马里","UsePostalcode":false},{"Id":135,"Name":"MM 缅甸","UsePostalcode":false},{"Id":136,"Name":"MN 蒙古","UsePostalcode":true},{"Id":137,"Name":"MO 澳门","UsePostalcode":false},{"Id":138,"Name":"MP 塞班岛","UsePostalcode":true},{"Id":139,"Name":"MQ 马提尼克岛","UsePostalcode":true},{"Id":140,"Name":"MR 毛里塔尼亚","UsePostalcode":false},{"Id":141,"Name":"MS 蒙特塞拉特岛","UsePostalcode":false},{"Id":142,"Name":"MT 马耳他","UsePostalcode":false},{"Id":143,"Name":"MU 毛里求斯","UsePostalcode":false},{"Id":144,"Name":"MV 马尔代夫","UsePostalcode":true},{"Id":145,"Name":"MW 马拉维","UsePostalcode":false},{"Id":146,"Name":"MX 墨西哥","UsePostalcode":true},{"Id":147,"Name":"MY 马来西亚","UsePostalcode":true},{"Id":148,"Name":"MZ 莫桑比克","UsePostalcode":false},{"Id":149,"Name":"NA 纳米比亚","UsePostalcode":false},{"Id":150,"Name":"NC 新喀里多尼亚","UsePostalcode":true},{"Id":151,"Name":"NE 尼日尔","UsePostalcode":false},{"Id":152,"Name":"NG 尼日利亚","UsePostalcode":false},{"Id":153,"Name":"NI 尼加拉瓜","UsePostalcode":false},{"Id":154,"Name":"NL 荷兰","UsePostalcode":true},{"Id":155,"Name":"NO 挪威","UsePostalcode":true},{"Id":156,"Name":"NP 尼泊尔","UsePostalcode":false},{"Id":157,"Name":"NR 瑙鲁","UsePostalcode":false},{"Id":158,"Name":"NU 纽埃岛","UsePostalcode":false},{"Id":159,"Name":"NZ 新西兰","UsePostalcode":true},{"Id":160,"Name":"OM 阿曼","UsePostalcode":false},{"Id":161,"Name":"PA 巴拿马","UsePostalcode":false},{"Id":162,"Name":"PE 秘鲁","UsePostalcode":false},{"Id":163,"Name":"PF 塔希提","UsePostalcode":true},{"Id":164,"Name":"PG 巴布亚新几内亚","UsePostalcode":true},{"Id":165,"Name":"PH 菲律宾","UsePostalcode":true},{"Id":166,"Name":"PK 巴基斯坦","UsePostalcode":true},{"Id":167,"Name":"PL 波兰","UsePostalcode":true},{"Id":168,"Name":"PR 波多黎各","UsePostalcode":true},{"Id":169,"Name":"PT 葡萄牙","UsePostalcode":true},{"Id":170,"Name":"PW 帕劳","UsePostalcode":true},{"Id":171,"Name":"PY 巴拉圭","UsePostalcode":false},{"Id":172,"Name":"QA 卡塔尔","UsePostalcode":false},{"Id":173,"Name":"RE 留尼旺岛","UsePostalcode":true},{"Id":174,"Name":"RO 罗马尼亚","UsePostalcode":true},{"Id":175,"Name":"RS 塞尔维亚共和国","UsePostalcode":true},{"Id":176,"Name":"RU 俄罗斯","UsePostalcode":true},{"Id":177,"Name":"RW 卢旺达","UsePostalcode":false},{"Id":178,"Name":"SA 沙特阿拉伯","UsePostalcode":false},{"Id":179,"Name":"SB 所罗门群岛","UsePostalcode":false},{"Id":180,"Name":"SC 塞舌尔","UsePostalcode":false},{"Id":181,"Name":"SD 苏丹","UsePostalcode":false},{"Id":182,"Name":"SE 瑞典","UsePostalcode":true},{"Id":183,"Name":"SG 新加坡","UsePostalcode":true},{"Id":184,"Name":"SH 圣赫勒拿岛","UsePostalcode":false},{"Id":185,"Name":"SI 斯洛文尼亚","UsePostalcode":true},{"Id":186,"Name":"SK 斯洛伐克","UsePostalcode":true},{"Id":187,"Name":"SL 塞拉利昂","UsePostalcode":false},{"Id":188,"Name":"SM 圣马力诺","UsePostalcode":true},{"Id":189,"Name":"SN 塞内加尔","UsePostalcode":false},{"Id":190,"Name":"SO 索马里","UsePostalcode":false},{"Id":191,"Name":"SR 苏里南","UsePostalcode":false},{"Id":192,"Name":"SS 南苏丹共和国","UsePostalcode":false},{"Id":193,"Name":"ST 圣多美和普林西比","UsePostalcode":false},{"Id":194,"Name":"SV 萨尔瓦多","UsePostalcode":false},{"Id":195,"Name":"SY 叙利亚","UsePostalcode":false},{"Id":196,"Name":"SZ 斯威士兰","UsePostalcode":true},{"Id":197,"Name":"TC 特克斯和凯科斯群岛","UsePostalcode":false},{"Id":198,"Name":"TD 乍得","UsePostalcode":false},{"Id":199,"Name":"TG 多哥","UsePostalcode":false},{"Id":200,"Name":"TH 泰国","UsePostalcode":true},{"Id":201,"Name":"TJ 塔吉克斯坦","UsePostalcode":true},{"Id":202,"Name":"TL 东帝汶","UsePostalcode":false},{"Id":203,"Name":"TN 突尼斯","UsePostalcode":true},{"Id":204,"Name":"TO 汤加","UsePostalcode":false},{"Id":205,"Name":"TR 土耳其","UsePostalcode":true},{"Id":206,"Name":"TT 特立尼达和多巴哥","UsePostalcode":false},{"Id":207,"Name":"TV 图瓦卢","UsePostalcode":false},{"Id":208,"Name":"TW 台湾","UsePostalcode":true},{"Id":209,"Name":"TZ 坦桑尼亚","UsePostalcode":false},{"Id":210,"Name":"UA 乌克兰","UsePostalcode":true},{"Id":211,"Name":"UG 乌干达","UsePostalcode":false},{"Id":212,"Name":"US 美国","UsePostalcode":true},{"Id":213,"Name":"UY 乌拉圭","UsePostalcode":false},{"Id":214,"Name":"UZ 乌兹别克斯坦","UsePostalcode":true},{"Id":215,"Name":"VC 圣文森特岛","UsePostalcode":false},{"Id":216,"Name":"VE 委内瑞拉","UsePostalcode":false},{"Id":217,"Name":"VG 英属维尔京群岛","UsePostalcode":false},{"Id":218,"Name":"VI 美属维尔京群岛","UsePostalcode":true},{"Id":219,"Name":"VN 越南","UsePostalcode":false},{"Id":220,"Name":"VU 瓦努阿图","UsePostalcode":false},{"Id":221,"Name":"WS 萨摩亚","UsePostalcode":false},{"Id":222,"Name":"XA ","UsePostalcode":false},{"Id":223,"Name":"XB 博内尔岛","UsePostalcode":false},{"Id":224,"Name":"XC 库拉索","UsePostalcode":false},{"Id":225,"Name":"XE 圣尤斯达求斯","UsePostalcode":false},{"Id":226,"Name":"XG ","UsePostalcode":false},{"Id":227,"Name":"XL 新西兰群岛领地（库克群岛） ","UsePostalcode":false},{"Id":228,"Name":"XM 圣马丁岛","UsePostalcode":false},{"Id":229,"Name":"XN 尼维斯岛","UsePostalcode":false},{"Id":230,"Name":"XS 索马里兰","UsePostalcode":false},{"Id":231,"Name":"XX ","UsePostalcode":false},{"Id":232,"Name":"XY 圣巴夫林米","UsePostalcode":true},{"Id":233,"Name":"YE 也门","UsePostalcode":false},{"Id":234,"Name":"YT 马约特岛","UsePostalcode":true},{"Id":235,"Name":"ZA 南非","UsePostalcode":true},{"Id":236,"Name":"ZM 赞比亚","UsePostalcode":false},{"Id":237,"Name":"ZW 津巴布韦","UsePostalcode":false},{"Id":238,"Name":"FP 法属波利尼西亚","UsePostalcode":false},{"Id":239,"Name":"TM 土庫曼","UsePostalcode":false},{"Id":240,"Name":"BQ ","UsePostalcode":false},{"Id":241,"Name":"VA 梵蒂冈","UsePostalcode":false},{"Id":242,"Name":"MF 法属圣马丁","UsePostalcode":false},{"Id":243,"Name":"PS 巴勒斯坦自治区","UsePostalcode":false},{"Id":244,"Name":"GBN 北爱尔兰","UsePostalcode":false},{"Id":245,"Name":"NF 诺褔克島","UsePostalcode":false}]';
            $rdata = json_decode($json_data, true);

            foreach ($rdata as $val){
                $item = array();
                $item['id'] = $val['Id'];
                $namearr = explode(' ', $val['Name']);
                $item['name_lt'] = $namearr[0];
                $item['name_en'] = '';
                $item['name_cn'] = $namearr[1];
                $data[] = $item;
            }
            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取升蓝物流价格查询列表
     * @param $country_id
     * @param int $actual_weight //单位：KG
     * @param int $volume_weight //单位：KG
     * @param $transport_id
     * @param $product_type
     * @return string
     */
    public function fetchSl($country_id, $actual_weight = 0, $volume_weight = 0, $transport_id = 0, $product_type = 1){

        $this->ql->post('https://www.sl56.com/Account/LogOn',[
            'Username' => 'CMHGZ8',
            'Password' => 'homary123'
        ])->get('https://signalr.sl56.com/signalr/hubs');
        $this->ql->post('https://www.sl56.com/Agreement/Agree');
        $this->ql->get('https://www.sl56.com/Calculation');

//        form表单 精简模式
//        https://www.sl56.com/Calculation/Calculate
//        modeOfTransportId: 1
//        productType: 1
//        countryId: 2
//        actualWeight: 100
//        volumeWeight:
//        declaredValue:
//        city:
//        postalCode:
//        volumetric:
//        selectedAttributeIdList:

        $post_data = [
            'modeOfTransportId' => $transport_id,
            'productType' => $product_type,
            'countryId' => $country_id,
            'actualWeight' => $actual_weight,
            'volumeWeight' => $volume_weight,
            'declaredValue' => '',
            'city' => '',
            'postalCode' => '',
            'volumetric' => '',
            'selectedAttributeIdList' => ''
        ];
        $this->ql->post('https://www.sl56.com/Calculation/Calculate', $post_data);
//        $rules = [
//            'name' => ['#headingOne > div > div.col-md-5 > h4 > a', 'text'],
//            'transit_method' => ['#headingOne > div > div.col-md-2', 'text'],
//            'total_price' => ['#headingOne > div > div:nth-child(3)', 'text'],
//            'money_type' => ['#headingOne > div > div:nth-child(4)', 'text'],
//            'weight' => ['#headingOne > div > div:nth-child(5)', 'text'],
//            'volume_div' => ['#headingOne > div > div:nth-child(6)', 'text'],
//            'plus_price' => ['#headingOne > div > div:nth-child(7)', 'text']
//        ];
//        $qdata = $this->ql->rules($rules)->range('#headingOne > div > div.col-md-5 > h4 > a')->queryData();
        $names = $this->ql->find('#headingOne > div > div:nth-child(1)')->texts()->toArray();
        $transit_methods = $this->ql->find('#headingOne > div > div.col-md-2')->texts()->toArray();
        $total_prices = $this->ql->find('#headingOne > div > div:nth-child(3)')->texts()->toArray();
        $money_types = $this->ql->find('#headingOne > div > div:nth-child(4)')->texts()->toArray();
        $weights = $this->ql->find('#headingOne > div > div:nth-child(5)')->texts()->toArray();
        $volume_divs = $this->ql->find('#headingOne > div > div:nth-child(6)')->texts()->toArray();
        $plus_prices = $this->ql->find('#headingOne > div > div:nth-child(7)')->texts()->toArray();
        $data = array();
        for ($i = 1; $i < count($names); $i++){
            $data[$i]['channel'] = '升蓝';
            $data[$i]['name'] = $names[$i];
            $data[$i]['transit_method'] = $transit_methods[$i];
            $data[$i]['weight'] = $weights[$i];
            $data[$i]['total_price'] = $total_prices[$i];
//            $data[$i]['money_type'] = $money_types[$i];
//            $data[$i]['volume_div'] = $volume_divs[$i];
//            $data[$i]['plus_price'] = $plus_prices[$i];
            $data[$i]['note'] = '';
        }

        return $data;
    }

    /**
     * 获取均辉物流国家
     */
    public function fetchJhCountries(){

        $data = array();
        $hashkey = md5('fetchJhCountries');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('http://exp.hecny.com/operlogin',[
                'userid' => 'JZGZ_VIP',
                'password' => 'JYJwuliu12345'
            ])->get('http://exp.hecny.com/straight/PriceList.shtml');
            $html = $this->ql->find('#countryPrice>option')->htmls();
            $rdata = $html->toArray();
            array_shift($rdata);

            foreach ($rdata as $val){
                $item = array();
                $namearr = explode(' ', $val);
                $item['id'] = '';
                $item['name_lt'] = $namearr[0];
                $item['name_cn'] = $namearr[1];
                array_shift($namearr);
                array_shift($namearr);
                $item['name_en'] = implode(' ', $namearr);
                $data[] = $item;
            }

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取均辉物流包裹类型
     */
    public function fetchJhTypes(){

        $data = array();
        $hashkey = md5('fetchJhTypes');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('http://exp.hecny.com/operlogin',[
                'userid' => 'JZGZ_VIP',
                'password' => 'JYJwuliu12345'
            ])->get('http://exp.hecny.com/straight/PriceList.shtml');
            $texts = $this->ql->find('#awbList > div > span:nth-child(2) > select > option')->texts();
            $vals = $this->ql->find('#awbList > div > span:nth-child(2) > select > option')->attrs('value');
            $texts = $texts->toArray();
            $vals = $vals->toArray();
            $data = array_combine($vals, $texts);

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取均辉物流渠道类型
     */
    public function fetchJhChannels(){

        $data = array();
        $hashkey = md5('fetchJhChannels');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('http://exp.hecny.com/operlogin',[
                'userid' => 'JZGZ_VIP',
                'password' => 'JYJwuliu12345'
            ])->get('http://exp.hecny.com/straight/PriceList.shtml');
            $texts = $this->ql->find('#awbList > div > span:nth-child(3) > select > option')->texts();
            $vals = $this->ql->find('#awbList > div > span:nth-child(3) > select > option')->attrs('value');
            $texts = $texts->toArray();
            $vals = $vals->toArray();
            $data = array_combine($vals, $texts);

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取均辉物流查询价格列表
     * @param string $countCode 国家简称
     * @param int $weight 重量(kg)
     * @param int $hawbTypeId 包裹类型
     * @param string $type 渠道类型
     */
    public function fetchJh($countCode = '', $weight = 0, $hawbTypeId = 2, $type = 'ALL'){

        $this->ql->post('http://exp.hecny.com/operlogin',[
            'name' => 'JZGZ_VIP',
            'pwd' => 'JYJwuliu12345'
        ])->get('http://exp.hecny.com/straight/PriceList.shtml');

//        form表单
//        http://exp.hecny.com/straight/straightBestPrice/list.action
//        countCode: AF
//        hawbTypeId: 2
//        type: ALL
//        wt: 100
        $post_data = [
            'countCode' => $countCode,
            'wt' => $weight,
            'hawbTypeId' => $hawbTypeId,
            'type' => $type,
        ];
        $this->ql->post('http://exp.hecny.com/straight/straightBestPrice/list.action', $post_data);
//        $rules = [
//            'id' => ['body > div > form:nth-child(2) > div > table > tbody > .text-c > td:nth-child(1)', 'text'],
//            'transit_method' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(2)', 'text'],
//            'transit_name' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(3)', 'text'],
//            'country_no' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(4)', 'text'],
//            'weight' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(5)', 'text'],
//            'price' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(6)', 'text'],
//            'rank_fee' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(7)', 'text'],
//            'burn_rate' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(8)', 'text'],
//            'store_rank_fee' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(9)', 'text'],
//            'emergency_fee' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(10)', 'text'],
//            'gas_fee' => ['body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(11)', 'text'],
//        ];
//        $qdata = $this->ql->rules($rules)->range('td')->queryData();
        $ids = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > .text-c > td:nth-child(1)')->texts()->toArray();
        $transit_methods = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(2)')->texts()->toArray();
        $transit_names = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(3)')->texts()->toArray();
        $country_nos = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(4)')->texts()->toArray();
        $weights = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(5)')->texts()->toArray();
        $prices = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(6)')->texts()->toArray();
        $rank_fees = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(7)')->texts()->toArray();
//        $burn_rates = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(8)')->texts()->toArray();
//        $store_rank_fees = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(9)')->texts()->toArray();
//        $emergency_fees = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(10)')->texts()->toArray();
        $gas_fees = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr > td:nth-child(11)')->texts()->toArray();
        $notes = $this->ql->find('body > div > form:nth-child(2) > div > table > tbody > tr.text-l > td')->texts()->toArray();

        $data = array();
        for ($i = 0; $i < count($ids); $i++){
            $data[$i]['channel'] = '均辉';
//            $data[$i]['id'] = $ids[$i];
            $data[$i]['name'] = $transit_names[$i];
            $data[$i]['transit_method'] = $transit_methods[$i];
//            $data[$i]['country_no'] = $country_nos[$i];
            $data[$i]['weight'] = number_format($weights[$i], 2);
//            $data[$i]['price'] = $prices[$i];
//            $data[$i]['rank_fee'] = $rank_fees[$i];
//            $data[$i]['burn_rate'] = $burn_rates[$i];
//            $data[$i]['store_rank_fee'] = $store_rank_fees[$i];
//            $data[$i]['emergency_fee'] = $emergency_fees[$i];
            $data[$i]['total_price'] = round(floatval($gas_fees[$i]), 2);
            $data[$i]['note'] = $notes[$i];
        }

        return $data;
    }

    /**
     * 获取出口易发货地址列表
     */
    public function fetchCkyFroms(){

        $data = array();
        $hashkey = md5('fetchCkyFroms');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->get('http://www.chukou1.com/Tool/NewShipping.aspx');
            $values = $this->ql->find('#from > option')->texts();
            $keys= $this->ql->find('#from > option')->attrs('value');
            $values = $values->toArray();
            $keys = $keys->toArray();
            $data = array_combine($keys, $values);

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取出口易国家列表
     */
    public function fetchCkyCountries(){

        $data = array();
        $hashkey = md5('fetchCkyCountries');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->get('http://www.chukou1.com/Tool/NewShipping.aspx');
            $html = $this->ql->find('#toCountry > optgroup:nth-child(2) > option')->htmls();
            $rdata = $html->toArray();

            foreach ($rdata as $val){
                $item = $namearr = array();
                $item['id'] = '';
                $namearr = explode(' ', $val);
                $item['name_lt'] = '';
                $item['name_cn'] = array_pop($namearr);
                $item['name_en'] = implode(' ', $namearr);
                $data[] = $item;
            }

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 获取出口易物流价格列表
     * @param string $toCountry
     * @param int $weight 单位：千克
     * @param string $from
     * @param int $length 单位：厘米
     * @param int $width 单位：厘米
     * @param int $height 单位：厘米
     * @return string
     */
    public function fetchCky($toCountry = '', $weight = 0, $from = 'GZ', $length = 1, $width = 1, $height = 1){

//        form表单
//        https://www.chukou1.com/Tool/ShippingHandler.ashx
//        token: ISJFDS09124LJ
//        from: SZ
//        toCountry: France
//        inpProvince:
//        inpZip: 0
//        inpCity:
//        weight: 1
//        l: 1.0
//        w: 1.0
//        h: 1.0
        $post_data = [
            'token' => 'ISJFDS09124LJ',
            'from' => $from,
            'toCountry' => $toCountry,
            'inpProvince' => '',
            'inpZip' => 0,
            'inpCity' => '',
            'weight' => $weight,
            'l' => $length,
            'w' => $width,
            'h' => $height
        ];

        $this->ql->post('https://www.chukou1.com/Tool/ShippingHandler.ashx', $post_data);

        $transit_names = $this->ql->find('#tab_fee > tr > td:nth-child(1) > b')->htmls()->toArray();
        $transit_codes = $this->ql->find('#tab_fee > tr > td:nth-child(2)')->htmls()->toArray();
        array_shift($transit_codes);
        $transit_fees = $this->ql->find('#tab_fee > tr > td:nth-child(3) > div')->htmls()->toArray();
        $other_fees = $this->ql->find('#tab_fee > tr > td:nth-child(4) > div')->htmls()->toArray();
        $total_fees = $this->ql->find('#tab_fee > tr > td:nth-child(5) > b')->htmls()->toArray();

        $data = array();
        if(!empty($transit_names)) {
            for ($i = 0; $i < count($transit_names); $i++) {
                $data[$i]['channel'] = '出口易';
                $data[$i]['name'] = $transit_names[$i];
                $data[$i]['transit_method'] = $transit_codes[$i];
                $data[$i]['weight'] = $weight;
//            $data[$i]['transit_fee'] = substr($transit_fees[$i], 5, strpos($transit_fees[$i], '<') - 5);
//            $data[$i]['other_fee'] = substr($other_fees[$i], 5, strpos($other_fees[$i], '<') - 5);
                $data[$i]['total_price'] = substr($total_fees[$i], 5);
                $data[$i]['note'] = '';
            }
        }
        return $data;
    }

    /**
     * 获取光速时代物流价格列表
     */
    public function fetchGsCountries(){
        $data = array();
        $hashkey = md5('fetchGsCountries');
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else{
            $this->ql->post('http://szgs.kingtrans.net/nclient/Logon?action=logon',[
                'userid' => 'GZJYJ',
                'password' => 'homary123'
            ])->get('http://szgs.kingtrans.net/nclient/CClientPrice?action=getAnalyse');
            $html = $this->ql->find('#searchForm > table > tr:nth-child(1) > td:nth-child(2) > select > option')->htmls();
            $rdata = $html->toArray();

            foreach ($rdata as $key => $val){
                if($key > 1){
                    $item = $namearr = array();
                    $item['id'] = $key;
                    $namearr = explode('-', $val);
                    $item['name_lt'] = $namearr[0];
                    $item['name_cn'] = $namearr[1];
                    $item['name_en'] = '';
                    $data[] = $item;
                }
            }

            \Redis::setex($hashkey, 3600, json_encode($data));
        }
        return $data;
    }

    /**
     * 查收光速时代物流价格列表
     * @param $country_no
     * @param $weight
     * @param string $goods_type
     */
    public function fetchGs($country_no, $weight, $goods_type = 'WPX'){

        $this->ql->post('http://szgs.kingtrans.net/nclient/Logon?action=logon',[
            'userid' => 'GZJYJ',
            'password' => 'homary123'
        ])->get('http://szgs.kingtrans.net/nclient/CClientPrice?action=getAnalyse');
//        form表单
//        http://szgs.kingtrans.net/nclient/CClientPrice?action=getAnalyse&pageflag=1
//        orgLookup.country: AU
//        weight: 100
//        rwtype: 0
//        len:
//        width:
//        height:
//        cc_analyse_price_rweights:
//        cc_analyse_price_lths:
//        cc_analyse_price_wths:
//        cc_analyse_price_hths:
//        cc_analyse_price_nums:
//        rwvalue:
//        goodstype: WPX
//        showDataType: 1
//        ptypeid:
//        city:
//        logistic:
//        post:
        $post_data = [
            'orgLookup.country' => $country_no,
            'weight' => $weight,
            'rwtype' => 0,
            'goodstype' => $goods_type,
            'showDataType' => 1
        ];
        $this->ql->post('http://szgs.kingtrans.net/nclient/CClientPrice?action=getAnalyse&pageflag=1', $post_data);

        $transit_names = $total_fees = $names = array();
        $transit_names = $this->ql->find('body > div.page > div > table > tbody > tr > td:nth-child(1)')->htmls()->toArray();
        $total_fees = $this->ql->find('body > div.page > div > table > tbody > tr > td:nth-child(5)')->htmls()->toArray();
        if(!empty($transit_names)){
            foreach($transit_names as $key => $item){
                if($key % 5 === 0 && $item != '无记录'){
                    $names[] = $item;
                }
            }
        }
        $data = array();
        if(!empty($names)) {
            for ($i = 0; $i < count($names); $i++) {
                $data[$i]['channel'] = '光速时代';
                $data[$i]['name'] = isset($names[$i]) ? $names[$i] : '';
                $data[$i]['transit_method'] = '';
                $data[$i]['weight'] = $weight;
                $data[$i]['total_price'] = isset($total_fees[$i]) ? $total_fees[$i] : 0;
                $data[$i]['note'] = '';
            }
        }
        return $data;
    }

    /**
     * 获取EMS单价记录
     * 用于计算EMS总价
     * @param string $country_name
     */
    private function getEmsPrice($country_name){
        //单价列表
        $sdata = [
            '1' => [
                'countries' => "澳门，台湾，香港",
                'doc' => 90,
                'pak' => 130,
                'plus' => 30
            ],
            '2' => [
                'countries' => "朝鲜，韩国，日本",
                'doc' => 115,
                'pak' => 180,
                'plus' => 40
            ],
            '3' => [
                'countries' => "菲律宾，柬埔寨，马来西亚，蒙古，泰国，新加坡，印度尼西亚，越南",
                'doc' => 130,
                'pak' => 190,
                'plus' => 45
            ],
            '4' => [
                'countries' => "澳大利亚，巴布亚新几内亚，新西兰",
                'doc' => 160,
                'pak' => 210,
                'plus' => 55
            ],
            '5' => [
                'countries' => "美国",
                'doc' => 180,
                'pak' => 240,
                'plus' => 75
            ],
            '6' => [
                'countries' => "爱尔兰，奥地利，比利时，丹麦，德国，法国，芬兰，荷兰，加拿大，卢森堡，马耳他，南非，挪威，葡萄牙，瑞典，瑞士，西班牙，希腊，意大利，英国",
                'doc' => 220,
                'pak' => 280,
                'plus' => 75
            ],
            '7' => [
                'countries' => "巴基斯坦，老挝，孟加拉，缅甸，尼泊尔，斯里兰卡，土耳其，印度",
                'doc' => 240,
                'pak' => 300,
                'plus' => 80
            ],
            '8' => [
                'countries' => "阿根廷，阿联酋，巴拿马，巴西，白俄罗斯，波兰，俄罗斯，哥伦比亚，古巴，圭亚那，捷克，秘鲁，墨西哥，乌克兰，匈牙利，以色列，约旦",
                'doc' => 260,
                'pak' => 335,
                'plus' => 100
            ],
            '9' => [
                'countries' => "阿曼，埃及，埃塞俄比亚，爱沙尼亚，巴林，保加利亚，博茨瓦纳，布基纳法索，刚果，哈萨克斯坦，吉布提，几内亚，加纳，加蓬，卡塔尔，开曼群岛，科特迪瓦，科威特，克罗地亚，肯尼亚，拉脱维亚，卢旺达，罗马尼亚，马达加斯加，马里，摩洛哥，莫桑比克，尼日尔，尼日利亚，塞内加尔，塞浦路斯，沙特阿拉伯，突尼斯，乌干达，叙利亚，伊朗，乍得",
                'doc' => 370,
                'pak' => 445,
                'plus' => 120
            ]
        ];
        $result = array();
        if(!empty($country_name)){
            foreach($sdata as &$item){
                if(strpos($item['countries'], $country_name) !== false){
                    $result = $item;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 获取EMS价格列表
     * @param $country_name
     * @param $weight (千克)
     * @param int $is_countable 是否计泡：计泡折扣0.7，不计泡折扣0.62
     */
    public function fetchEms($country_name, $weight, $is_countable = 0){

        $data = $record = array();
        $record = $this->getEmsPrice($country_name);
        if(!empty($record)){
            $total_price = 0;
            $discount = !empty($is_countable) ? 0.7 : 0.62;
            $plus_price = !empty($is_countable) ? 50 : 20; //为泡货时50，否则20
            $weight = $weight * 1000; //转为克
            if($weight > 500) {
                //EMS公式 = （实重 - 500） / 500 * 续重价格 + 首重价格 * 折扣 + 50|20 单位：克
                $total_price = (($weight - 500) / 500 * $record['plus'] + $record['pak']) * $discount  + $plus_price;
//                echo '(('.$weight.' - 500) / 500 * '.$record['plus'].' + '.$record['pak'].') * '.$discount.'  + '.$plus_price;
//                dd($total_price);
            }else{
                //EMS公式 = 首重价格 * 折扣 + 50|20
                $total_price = $record['pak'] * $discount + $plus_price;
//                echo $record['pak'].' * '.$discount.'  + '.$plus_price;
//                dd($total_price);
            }
            $item = array();
            $item['channel'] = 'EMS';
            $item['name'] = 'EMS';
            $item['transit_method'] = 'EMS';
            $item['weight'] = $weight / 1000;
            $item['total_price'] = $total_price;
            $item['note'] = '';
            $data[] = $item;
        }
        return $data;
    }

    /**
     * 获取参与计算的物流重量
     * 以0.5KG为单位向上取值
     * @param $weight
     */
    private function _getTransitWeight($weight){

        $warr = explode('.', $weight);
        if(count($warr) > 1){
            $ltnum =  floatval('0.' . $warr[1]);
            $ltnum = $ltnum > 0 && $ltnum <= 0.5 ? 0.5 : 1;
            $weight = $warr[0] + $ltnum;
        }
        return $weight;
    }

    /**
     * 查询所有的物流价格列表
     * @param $country_name
     * @param $actual_weight
     * @param $volume_weight
     * @param string $transit_method
     * @param int $length
     * @param int $width
     * @param int $height
     * @param int $volume_div
     * @param int $is_countable
     * @return array|mixed
     */
    public function fetchDatas($country_name, $actual_weight, $volume_weight, $transit_method = '', $length = 0, $width = 0, $height = 0, $volume_div = 5000, $is_countable = 0){
        $data = array();
        $hashkey = md5('fetchDatas'.'-'.$country_name.'-'.$actual_weight.'-'.$volume_weight.'-'.$transit_method.'-'.$is_countable);
        if(\Redis::exists($hashkey)){
            $jsonstr = \Redis::get($hashkey);
            $data = isset($jsonstr) ? json_decode($jsonstr, true) : array();
        }else {
            $ydlist = $sllist = $jhlist = $chklist = $emslist = $gslist = array();
            $aweight = $this->_getTransitWeight($actual_weight);
            $vweight = $this->_getTransitWeight($volume_weight);
            $weight = $aweight > $vweight ? $aweight : $vweight;
            //获取毅达物流价格列表
            $country_no = '';
            $countries = $this->fetchYdCountries();
            foreach ($countries as &$item) {
                if ($item['name_cn'] == $country_name) {
                    $country_no = $item['name_lt'];
                    break;
                }
            }
            $ydlist = $this->fetchYd($country_no, $weight, $transit_method);

            //获取升蓝物流价格列表
            $country_id = 0;
            $countries = $this->fetchSlCountries();
            if(!empty($countries)) {
                foreach ($countries as &$item) {
                    if ($item['name_cn'] == $country_name) {
                        $country_id = $item['id'];
                        break;
                    }
                }
            }
            $transport_id = 0;
            $transit_methods = $this->fetchSlMethods();
            if(!empty($transit_methods)){
                foreach($transit_methods as $key=>$val){
                    if($transit_method == $val) {
                        $transport_id = $key;
                        break;
                    }
                }
            }

            $sllist = $this->fetchSl($country_id, $weight, 0, $transport_id);

            //获取均辉物流价格列表
            $countCode = '';
            $countries = $this->fetchJhCountries();
            foreach ($countries as &$item) {
                if ($item['name_cn'] == $country_name) {
                    $countCode = $item['name_lt'];
                    break;
                }
            }
            $jhlist = $this->fetchJh($countCode, $weight);

            if(empty($transit_method) || $transit_method = 'EMS') {
                $emslist = $this->fetchEms($country_name, $aweight, $is_countable);
            }
            //获取光速时代物流价格列表
            $volume_weight = round($length * $width * $height / 6000, 2);
            $weight = $actual_weight > $volume_weight ? $actual_weight : $volume_weight;
            $country_no = '';
            $countries = $this->fetchGsCountries();
            foreach ($countries as &$item) {
                if ($item['name_cn'] == $country_name) {
                    $country_no = $item['name_lt'];
                    break;
                }
            }
            $gslist = $this->fetchGs($country_no, $weight);

            //获取出口易物流价格列表
//            $toCountry = '';
//            $countries = $this->fetchCkyCountries();
//            foreach ($countries as &$item) {
//                if (!strpos($item['name_cn'], $country_name)) {
//                    $toCountry = $item['name_en'];
//                    break;
//                }
//            }
//            $chklist = $this->fetchCky($toCountry, $weight);

            $data = array_merge($ydlist, $sllist, $jhlist, $emslist, $gslist);
            array_multisort(array_column($data , 'total_price'), SORT_ASC, $data);

            \Redis::setex($hashkey, 600, json_encode($data));
        }
        return $data;
    }
}
