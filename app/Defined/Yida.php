<?php


namespace App\Defined;

use QL\QueryList;

class Yida
{
    /**
     * 获取国家参数列表
     */
    public function fetchCountries(){
        $ql = QueryList::post('http://yd.a-56.com:5903/userLogin/go',[
            'userid' => 'YX5075',
            'password' => 'YD123456'
        ])->get('http://yd.a-56.com:5903/Price/CalPriceExp');
//        form表单
//        module: Search
//        method: country
//        PageSize: 7
//        PageIndex: 1
//        first: 0
//        guest:
//        Company: YD
//        type: 1
        $post_data = [
            'module' => 'Search',
            'method' => 'country',
            'PageSize' => 1000,
            'PageIndex' => 1,
            'first' => 0,
            'guest' => '',
            'Company' => 'YD',
            'type' => 1
        ];
        $result = $ql->post('http://yd.a-56.com:5903/ashx/Search.ashx', $post_data);

        return $result;
    }
}
