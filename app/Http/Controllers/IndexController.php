<?php

namespace App\Http\Controllers;

use App\Services\ShippingService;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IndexController extends Controller
{
    private  $shipping_service;
    public function __construct(ShippingService $shipping_service)
    {
        $this->shipping_service = $shipping_service;
    }

    /**
     * 首页
     */
    public function index(){
//        return view('welcome', ['hello' => 'hello world']);

        $yd_countries = $this->shipping_service->fetchYdCountries();
        $ydmethods = $this->shipping_service->fetchYdMethods();
        array_pop($ydmethods);
        $slmethods = $this->shipping_service->fetchSlMethods();
        array_shift($slmethods);
        $methods = array_merge($ydmethods, $slmethods);
        $methods = array_unique($methods);

        return view('index', ['countries' => $yd_countries, 'methods' => $methods, 'list' => [], 'country_name' => '', 'actual_weight' => 0, 'volume_weight'=>0, 'transit_method' => '', 'is_countable' => 0, 'length' => 0, 'width' => 0, 'height' => 0, 'volume_div' => 5000]);
    }

    /**
     * 获取所有的运输方式
     */
    public function fetchAllTransitMethods(){
        $ydmethods = $this->shipping_service->fetchYdMethods();
        array_pop($ydmethods);
        $slmethods = $this->shipping_service->fetchSlMethods();
        array_shift($slmethods);
        $methods = array_merge($ydmethods, $slmethods);
        $methods = array_unique($methods);
        return $this->success($methods);
    }

    /**
     * 获取所有物流的国家列表
     */
    public function fetchAllCountries(){
        $yd_countries = $this->shipping_service->fetchYdCountries();
//        $sl_countries = $this->shipping_service->fetchSlCountries();
//        $jh_countries = $this->shipping_service->fetchJhCountries();
//        $cky_countries = $this->shipping_service->fetchCkyCountries();
        return $this->success($yd_countries);
    }

    /**
     * 查询所有物流的价格
     * @param Request $request
     */
    public function query(Request $request){

        try{
            $country_name = trim(strip_tags($request->input('country_name', '')));
            $actual_weight = floatval($request->input('actual_weight', 0));
            $volume_weight = floatval($request->input('volume_weight', 0));
            $transit_method = trim(strip_tags($request->input('transit_method', '')));
            $is_countable = intval($request->input('is_countable', 0));
            $length = intval($request->input('length', 0));
            $width = intval($request->input('width', 0));
            $height = intval($request->input('height', 0));
            $volume_div = intval($request->input('volume_div', 0));

            if(empty($country_name) || (empty($actual_weight) && empty($volume_weight))) throw new \Exception("参数异常", 20001);
            $datas = $this->shipping_service->fetchDatas($country_name, $actual_weight, $volume_weight, $transit_method, $length, $width, $height, $volume_div, $is_countable);

            $yd_countries = $this->shipping_service->fetchYdCountries();
            $ydmethods = $this->shipping_service->fetchYdMethods();
            array_pop($ydmethods);
            $slmethods = $this->shipping_service->fetchSlMethods();
            array_shift($slmethods);
            $methods = array_merge($ydmethods, $slmethods);
            $methods = array_unique($methods);

            return view('index', ['list'=>$datas, 'countries' => $yd_countries, 'methods'=>$methods, 'country_name' => $country_name, 'actual_weight' => $actual_weight, 'volume_weight'=>$volume_weight, 'transit_method' => $transit_method, 'is_countable' => $is_countable, 'length' => $length, 'width' => $width, 'height' => $height, 'volume_div' => $volume_div]);
//            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取毅达运输方式
     */
    public function fetchYdMethods(){
        try {
            $data = $this->shipping_service->fetchYdMethods();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取毅达包裹类型
     */
    public function fetchYdTypes(){
        try{
            $data = $this->shipping_service->fetchYdTypes();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }
    /**
     * 获取物流目的地国家列表
     */
    public function fetchYdCountries(){
        try{
            $data = $this->shipping_service->fetchYdCountries();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取毅达物流价格查询列表
     */
    public function fetchYd(Request $request){

        try{
            $country_no = trim(strip_tags($request->input('country_no', '')));
            $weight = floatval($request->input('weight', 0));
            $pack_type = trim(strip_tags($request->input('pack_type', '')));
            $transit_method = trim(strip_tags($request->input('transit_method', '')));
            $data = $this->shipping_service->fetchYd('AD', 100, $pack_type, $transit_method);
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取升蓝运输方式
     */
    public function fetchSlMethods(){
        try {
            $data = $this->shipping_service->fetchSlMethods();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取升蓝货物类型
     */
    public function fetchSlTypes(){
        try{
            $data = $this->shipping_service->fetchSlTypes();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取升蓝国家列表
     */
    public function fetchSlCountries(){
        try{
            $data = $this->shipping_service->fetchSlCountries();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取升蓝物流价格查询列表
     */
    public function fetchSl(){
        try{
            $data = $this->shipping_service->fetchSl(0, 1, 1, 100);
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取均辉物流国家列表
     */
    public function fetchJhCountries(){
        try{
            $data = $this->shipping_service->fetchJhCountries();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取均辉物流包裹类型
     */
    public function fetchJhTypes(){
        try{
            $data = $this->shipping_service->fetchJhTypes();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取均辉物流渠道
     */
    public function fetchJhChannels(){
        try{
            $data = $this->shipping_service->fetchJhChannels();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取均辉物流价格查询列表
     */
    public function fetchJh(){
        try{
            $data = $this->shipping_service->fetchJh('CA', 100);
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取出口易发货源地址列表
     */
    public function fetchCkyFroms(){
        try{
            $data = $this->shipping_service->fetchCkyFroms();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取出口易国家列表
     */
    public function fetchCkyCountries(){
        try{
            $data = $this->shipping_service->fetchCkyCountries();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取出口易物流价格查询列表
     */
    public function fetchCky(){
        try{
            $data = $this->shipping_service->fetchCky('SZ', 'France', 1);
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取光速时代国家列表
     */
    public function fetchGsCountries(){

        try{
            $data = $this->shipping_service->fetchGsCountries();
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 湖区哦光速时代物流价格查询列表
     */
    public function fetchGs(){

        try{
            $data = $this->shipping_service->fetchGs('AU', 2.36);
            return $this->success($data);
        }catch (\Exception $e){
            return $this->result($e->getCode(), $e->getMessage());
        }
    }
}
