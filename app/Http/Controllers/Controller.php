<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 请求成功返回
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($data){
        $result = ['code' => 0, 'errmsg' => '', 'data' => $data];
        return response()->json($result);
    }

    /**
     * 请求返回结果
     * @param int $code
     * @param string $errmsg
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function result($code = 0, $errmsg = '', $data = array()) {
        $result = ['code' => $code, 'errmsg' => $errmsg, 'data' => $data];
        return response()->json($result);
    }
}
