<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="js/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <title>物流运费集成查询</title>
</head>
<body>
<div class="container mt-5">
    <div class="row">
        <h1 class="m-auto">物流运费集成查询</h1>
    </div>
</div>
<div class="container border p-5 mt-5" style="max-width: 1400px;">

    <form class="clearfix" action="/query" method="post" id="queryForm">
        @csrf
        <div class="row">
            <div class="form-group col-2">
                <label for="country">国家名称</label>

                <select class="form-control" id="country" data-live-search="true" name="country_name">
                    <option value="all">请选择</option>
                    @foreach($countries as $item)
                    <option @if($country_name == $item['name_cn']) selected @endif value="{{$item['name_cn']}}">{{$item['name_lt']}} {{ $item['name_cn']}} {{ $item['name_en']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-2">
                <label for="weight">重量(KG)</label>
                <input type="text" class="form-control" id="actual_weight" autocomplete="off" name="actual_weight" value="{{$actual_weight}}">
            </div>
            <div class="form-group col-2">
                <label for="weight">体积重(KG)</label>
                <input type="text" class="form-control" id="volume_weight" autocomplete="off" name="volume_weight" value="{{$volume_weight}}">
            </div>

            <div class="form-group col-1">
                <label for="length">长(CM)</label>
                <input type="text" class="form-control" autocomplete="off" id="length" name="length" value="{{$length}}" onfocusout="calculate()">
            </div>
            <div class="form-group col-1">
                <label for="width">宽(CM)</label>
                <input type="text" class="form-control" autocomplete="off" id="width" name="width" value="{{$width}}" onfocusout="calculate()">
            </div>
            <div class="form-group col-1">
                <label for="height">高(CM)</label>
                <input type="text" class="form-control" autocomplete="off" id="height" name="height" value="{{$height}}" onfocusout="calculate()">
            </div>

            <div class="form-group col-1">
                <label for="volumeDiv">材积类型</label>
                <select class="form-control" id="volumeDiv" data-live-search="true" name="volumeDiv" onchange="calculate()">
                    <option value="5000" @if($volume_div == 5000) selected @endif>5000</option>
                    <option value="5500" @if($volume_div == 5500) selected @endif>5500</option>
                    <option value="5800" @if($volume_div == 5800) selected @endif>5800</option>
                    <option value="6000" @if($volume_div == 6000) selected @endif>6000</option>
                    <option value="7000" @if($volume_div == 7000) selected @endif>7000</option>
                    <option value="8000" @if($volume_div == 8000) selected @endif>8000</option>
                    <option value="9000" @if($volume_div == 9000) selected @endif>9000</option>
                    <option value="10000" @if($volume_div == 10000) selected @endif>10000</option>
                </select>
            </div>

            <div class="form-group col-1">
                <label for="operation">运输方式</label>
                <select class="form-control" id="operation" data-provide="typeahead" name = "transit_method">
                    <option value="">请选择</option>
                    @foreach($methods as $item)
                    <option @if($item == $transit_method) selected @endif value="{{$item}}">{{$item}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-1">
                <label for="height">是否泡货</label>
                <select class="form-control" id="is_countable" data-live-search="true" name="is_countable">
                    <option value="0">否</option>
                    <option value="1" @if($is_countable > 0) selected @endif>是</option>
                </select>
            </div>
        </div>

        <div class="row" style="margin-top: 30px;">
            <button type="button" onclick="checkForm()" class="btn btn-primary" style="margin: 0 auto;">提交计算</button>
        </div>

    </form>

    <div class="row mt-5">
        <table class="table mt-5">
            <thead>
            <tr>
                <th scope="col">物流商名称</th>
                <th scope="col">渠道名称</th>
                <th scope="col">渠道代码</th>
                <th scope="col">重量（KG）</th>
                <th scope="col">费用</th>
            </tr>
            </thead>

            <tbody>
            @if(!empty($list))
                @foreach($list as $item)
                    <tr class="list" style="cursor:pointer;" onclick="showNote(this)">
                        <th scope="row">{{$item['channel']}}</th>
                        <td>{{$item['name']}}</td>
                        <td>{{$item['transit_method']}}</td>
                        <td>{{$item['weight']}}</td>
                        <td>{{$item['total_price']}}</td>
                    </tr>
                    <tr class="notes" style="display: none; background-color:lightgray;font-size:12px;">
                        <td colspan="5">{{$item['note']}}</td>
                    </tr>
                @endforeach
            @else
            @endif
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function () {
        $('#country').selectpicker();
    });
    function showNote(e){
        $('.notes').hide();
        $(e).next('.notes').show();
    }
    function calculate(){
        var length = $("#length").val();
        var width = Number($("#width").val());
        var height = Number($("#height").val());
        var volume_div = Number($("#volumeDiv").val());
        console.log(volume_div);
        var volume_weight = length * width * height / volume_div;
        volume_weight = volume_weight.toFixed(2);
        // console.log(volume_weight);
        if(volume_weight > 0) {
            // volume_weight.toString();
            // var numarr = String(volume_weight).split(".");
            // if(numarr.length > 1){
            //     var ltnum = Number("0." + numarr[1]);
            //     if(ltnum > 0 && ltnum <= 0.5){
            //         volume_weight = Number(numarr[0]+".5");
            //     }else{
            //         volume_weight = numarr[0] + 1;
            //     }
            // }
            $("#volume_weight").val(volume_weight);
        }
    }

    function checkForm(){
        var country_name = $("#country").val();
        var actual_weight = $("#actual_weight").val();
        var volume_weight = $("#volume_weight").val();

        if(country_name == '' || country_name == 'all' || (actual_weight == 0 && volume_weight ==0)){
            alert("参数不正确");
            return false;
        }else{
            var chkform = $("#queryForm");
            chkform.submit();
        }
    }
</script>
</body>
</html>
