<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/custom.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <title>ERP集成查询</title>
</head>
<body>
<div class="container mt-5">
    <div class="row">
        <h1 class="m-auto">ERP集成查询</h1>
    </div>
</div>
<div class="container border p-5 mt-5">

    <form class="form-inline" action="/erp/query" method="post" id="queryForm">
        @csrf
        <div class="form-group">
            <label for="country" style="font-weight: bold;">产品代码：</label>
            <input title="Barcode(多个产品代码，用英文逗号分隔。)" type="text" class="form-control" style="width: 500px;" id="barcode" complete="off" name="barcode" value="{{$barcode}}">
        </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" onclick="checkForm()" class="btn btn-primary">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="javascript:void(0);" onclick="exportList()">导出</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="javascript:void(0);" onclick="exportSkus()">导出组合产品库存</a>
    </form>

    <div class="row mt-5">
        <table class="table mt-5">
            <thead>
            <tr>
                <th scope="col">产品名称</th>
                <th scope="col">产品代号</th>
                <th scope="col">仓库</th>
                <th scope="col">采购在途</th>
                <th scope="col">可用/预警</th>
                <th scope="col">可售/可售天数</th>
                <th scope="col">缺货/缺货天数</th>
                <th scope="col">预计到货</th>
            </tr>
            </thead>

            <tbody>
            @if(!empty($list))
                @foreach($list as $item)
                    <tr class="list" style="cursor:pointer;"">
                        <th scope="row">{{$item['product_title']}}</th>
                        <td>{{$item['product_barcode']}}</td>
                        <td>{{$item['warehouse']}}</td>
                        <td>{{$item['pi_purchase_onway']}}</td>
                        <td>{{$item['pi_in_used']}}/{{$item['pi_warning_qty']}}</td>
                        <td>{{$item['pi_in_used']}}/{{$item['pi_can_sale_days']}}</td>
                        <td>{{$item['pi_no_stock']}}/{{$item['pi_no_stock_days']}}</td>
                        <td>{{$item['expected_date']}}</td>
                    </tr>
                @endforeach
            @else
            @endif
            </tbody>
        </table>
    </div>
</div>

<script>
    function checkForm(){
        var barcode = $("#barcode").val();

        if(barcode == ''){
            alert("参数不正确");
            return false;
        }else{
            var chkform = $("#queryForm");
            chkform.submit();
        }
    }
    function exportList(){

        var barcode = $("#barcode").val();

        if(barcode == ''){
            alert("参数不正确");
            return false;
        }
        window.location.href = "/erp/export?barcode="+barcode;
    }

    function exportSkus(){

        var barcode = $("#barcode").val();

        if(barcode == ''){
            alert("参数不正确");
            return false;
        }
        window.location.href = "/erp/exportSkus?barcode="+barcode;
    }
</script>
</body>
</html>
