<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::any('query', 'IndexController@query');
Route::get('fetchyd', 'IndexController@fetchYd');
Route::get('fetchYdCountries', 'IndexController@fetchYdCountries');
Route::get('fetchYdMethods', 'IndexController@fetchYdMethods');
Route::get('fetchYdTypes', 'IndexController@fetchYdTypes');

Route::get('fetchsl', 'IndexController@fetchSl');
Route::get('fetchSlTypes', 'IndexController@fetchSlTypes');
Route::get('fetchSlMethods', 'IndexController@fetchSlMethods');
Route::get('fetchSlCountries', 'IndexController@fetchSlCountries');

Route::get('fetchjh', 'IndexController@fetchJh');
Route::get('fetchJhCountries', 'IndexController@fetchJhCountries');
Route::get('fetchJhTypes', 'IndexController@fetchJhTypes');
Route::get('fetchJhChannels', 'IndexController@fetchJhChannels');

Route::get('fetchcky', 'IndexController@fetchCky');
Route::get('fetchCkyFroms', 'IndexController@fetchCkyFroms');
Route::get('fetchCkyCountries', 'IndexController@fetchCkyCountries');

Route::get('fetchgs', 'IndexController@fetchGs');
Route::get('fetchGsCountries', 'IndexController@fetchGsCountries');

Route::get('fetchAllTransitMethods', 'IndexController@fetchAllTransitMethods');
Route::get('fetchAllCountries', 'IndexController@fetchAllCountries');

Route::any('erp', 'ErpController@index');
Route::any('erp/query', 'ErpController@query');
Route::any('erp/export', 'ErpController@export');
Route::any('erp/exportSkus', 'ErpController@exportSkus');


